{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Util (addFeed) where

import TestImport

addFeed :: YesodExample App ()
addFeed = do
  request $ do
    setMethod "POST"
    setUrl ReadR
    addToken
    byLabelExact "URL" "https://joeyh.name/blog/index.rss"

