{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.ReadSpec (spec) where

import TestImport
import Util

spec :: Spec
spec = withApp $ do
  describe "Read page" $ do
    it "loads" $ do
      get ReadR
      statusIs 200
      htmlAnyContain "button" "add"

      -- Add a feed once
      addFeed

      statusIs 303

      -- check that it's displayed
      request $ do
        setMethod "GET"
        setUrl ReadR
      statusIs 200
      htmlAnyContain "a" "see shy jo"

      -- add the same feed a second time
      addFeed

      statusIs 303

      feeds <- runDB $ selectList ([] :: [Filter RFeed]) []
      print $ length feeds
      assertEq  "One feed in the DB" 1 (length feeds)

