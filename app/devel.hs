{-# LANGUAGE PackageImports #-}
import "reader" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
