{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Feed where

import Import

import qualified Data.Text as T
import Data.Char (isNumber)
import Data.Maybe (fromJust)
import qualified Database.Esqueleto  as E
import Database.Esqueleto ((^.))
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Prelude(read)

getFeedR :: RFeedId -> Handler Html
getFeedR fi = do

  -- items <- runDB
  --   $ E.select
  --   $ E.from $ \(rfeeditem `E.LeftOuterJoin` ritem) -> do
  --   E.on (rfeeditem ^. RFeedRItemItemid E.==. ritem ^. RItemId)
  --   E.where_ (rfeeditem ^. RFeedRItemFeedid E.==. (E.val fi))
  --   return
  --     (
  --      ritem ^. RItemId
  --     , ritem ^.  RItemTitle
  --     , ritem ^. RItemDescription
  --     )

  -- let focalItem = case items of
  --       ((i, t, d):[]) -> Just d
  --       _ -> Nothing

  defaultLayout $ do
    aDomId <- newIdent
    setTitle "Simple RSS Reader"
    $(widgetFile "feed")


getNewFeedR :: Handler Html
getNewFeedR = do
  feedMaybe <- lookupGetParam "feed"
  itemMaybe <- lookupGetParam "item"

  items <- case feedMaybe of
    Just feed -> do
      let fk = read $ T.unpack $ T.filter (isNumber) feed :: Int64
      runDB $ selectList ([RItemFeedid ==. (toSqlKey fk)] :: [Filter RItem]) []

  let feed = case feedMaybe of
        Just f -> f

  focalItems <- case itemMaybe of
        Just i -> do
          let ik = read $ T.unpack i :: Int64
          runDB $ selectList [RItemId ==. (toSqlKey ik)] []

  let focalItem = case focalItems of
        [x] -> x


  defaultLayout $ do
    aDomId <- newIdent
    setTitle "Simple RSS Reader"
    $(widgetFile"newfeed")
