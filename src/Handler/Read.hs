{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Read where

import Import
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3, bootstrapSubmit, BootstrapSubmit (..))
import Text.Julius (RawJS (..))

import Network.HTTP.Simple
import Text.XML
import Text.XML.Cursor



-- Define our data that will be used for creating the form.
data RFeedForm = RFeedForm
    {
      url :: Text
    }

data AddStates a b = NotStarted | FetchError a | FetchSuccess b
  deriving Eq


getReadR :: Handler Html
getReadR = do

    (formWidget, formEnctype) <- generateFormPost rFeedForm
    let submission = NotStarted :: AddStates Text RFeed
        handlerName = "getHomeR" :: Text

    allFeeds <- runDB $ getAllFeeds

    defaultLayout $ do
        aDomId <- newIdent
        setTitle "Simple RSS Reader"
        $(widgetFile "read")

-- TODO: need to use setMessage and then getMessage in defaultLayout to show notification about if adding feed successed or not.
-- see https://www.yesodweb.com/book/sessions#sessions_messages
postReadR :: Handler Html
postReadR = do
    ((result, formWidget), formEnctype) <- runFormPost rFeedForm
    case result of
      FormSuccess res -> do
        fetchResult <- fetchFeed (url res)
        case fetchResult of
          FetchSuccess (rfeed, ritems) -> do
            fiAttempt <- runDB $ insertBy rfeed
            case fiAttempt of
              Left r -> do
                setMessage "Feed already exists"
              Right fi -> do
                iis <- runDB $ insertMany $ map (\(t, d, u) -> RItem fi t d u) ritems
                -- _ <- runDB $ insertMany $ map (RFeedRItem fi) iis
                setMessage "Added feed"

            redirect ReadR


rFeedForm :: Form RFeedForm
rFeedForm = renderBootstrap3 BootstrapInlineForm $ RFeedForm
  <$> areq textField us  Nothing
  where
    us = FieldSettings {
      fsLabel = "URL"
      , fsTooltip = Nothing
      , fsId = Nothing
      , fsName = Nothing
      , fsAttrs =
          [ ("class", "form-control")
          , ("placeholder", "http://")
          ]
      }


-- fetchFeed' :: Text-> AddStates Text (RFeed, [RItem])
fetchFeed url = do

  req <- parseRequest (unpack url)
  r <- httpLBS req
  let d = parseLBS_ def (getResponseBody r)

  let c = fromDocument d

  -- get the 'channel' title
  let feedTitle = Import.concat $ c $/ element "channel" &/ element "title" &/ content
  let feedLink = Import.concat $ c $/ element "channel" &/ element "link" &/ content
  let feedDescription = Import.concat $ c $/ element "channel" &/ element "description" &/ content
  let rf = RFeed feedTitle feedDescription feedLink
  -- get all the titles:
  let titles =  c $/ element "channel" &// element "item" &// element "title" &// content
  let links =  c $/ element "channel" &// element "item" &// element "link" &// content
  let descriptions =  c $/ element "channel" &// element "item" &// element "description" &// content

  let items = Import.zip3 titles descriptions links

  return $ FetchSuccess (rf, items)


getAllFeeds :: DB [Entity RFeed]
getAllFeeds = selectList [] [Asc RFeedId]
