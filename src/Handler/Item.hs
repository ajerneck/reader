{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Item where

import Import
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3)
import Text.Julius (RawJS (..))

import qualified Database.Esqueleto  as E
import Database.Esqueleto ((^.))

getItemR :: RItemId -> Handler Html
getItemR itemId = do
  item <- runDB $ get404 itemId

  defaultLayout $ do
    aDomId <- newIdent
    setTitle "Simple RSS Reader"
    [whamlet|#{preEscapedToMarkup $ rItemDescription item}|]
