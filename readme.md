# Simple RSS Reader

## Releases

### 0.0.1

a list of feeds
read a feed
add a feed

#### Minimal features

X read all the feeds from the database and list them.
X * create a feed page, which lists all the items in a feed.


#### Authentication / users

- add user-feed subscription table (once we have user functionality).

* make read page go to login screen if not logged in.

#### UI

* Remove existing site elements and copy.
  - started, keeping some for convenience.

* ui nicetise
add <a href="#" to get nice mouseover.

* breadcrumbs so it's easy to get back to the preceding page 

* navigation on feed/item pages to go to next feed/item
(use the pager component)

* badge with number of unread (requires read table)

#### Bugs

X 1. feed page shows items from multiple (all?) feeds.
2. item for statmodeling feed shows only truncated text.

## Done

fetch at least one feed from the internet.
    - currently working in fetch.hs.
    - todo: need an URL type, check if there is one.
    - need to return a list of items, the items are to be normalized into a persistent table with items. ALso, when adding a feed to the DB:
      - add feed to feed table.
        - add uniqueness constraint: 
        
      - add items to items table.
      - add feed-item relationship table.
## Cancelled

* cleanup: create sum type for read submission: notStarted | Success | Failure.
   then we can pattern match in hamlet using `case` to show an alert on success and failure, but none on notStarted.
* basic functionality
  - use dynamic multi routes to get urls like feed/1/item/1, so that there's a webpage to actually read each item.



### 0.0.2

add a feed

## Features

### site
authenticate as a user.
unauthenticated:
browse popular feeds.

### feeds
add a feed via exact link.
rm a feed
periodically check for updates.

### items
read an item in a feed
list all (unread and read) items in a feed.
mark an item as read. (mark all items as read.

### Users
sign up.
delete account.

### search

### browse

### share

### tag

## Principles

There should be tests.
there should be observability

atomic commit, creating the entire server (maybe as a docker image?

log data

ci/cd


## Technologies

yesod, other haskell.

gitlab for ci/cd
